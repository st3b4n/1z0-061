--create table type_train(amount number(*,1))
begin
  declare
	 nbr_sample1 number := 123456789 ;
	 nbr_sample2 number(5,-4);
	 nl char(1) := chr(13);
	begin
	 dbms_output.put_line( '###################### Part2 - Sample 01 ###################' );
	 dbms_output.put_line( '# precision 5 scale -4	#' );
	 dbms_output.put_line( '# nearest 10,000 #' );
	 dbms_output.put_line( nl  );
	 dbms_output.put_line( 'Original value is 			: ' || cast( nbr_sample1 as varchar2) );
	 nbr_sample2 := nbr_sample1;
	 dbms_output.put_line( 'Value saved as number(5,-4) type	: ' || cast( nbr_sample2 as varchar2) );
    end;
 --********************************************************************************************************
  --********************************************************************************************************
  declare
	 nbr_sample1 number := 1234567890 ;
	 nbr_sample2 number(5,-4);
	 nl char(1) := chr(13);
	begin
	 dbms_output.put_line( '###################### Part2 - Sample 01 ###################' );
	 dbms_output.put_line( '# precision 5 scale -4	#' );
	 dbms_output.put_line( '# outside the range, only 5 digits + 4 trailing zeros  #' );
	 dbms_output.put_line( nl  );
	 dbms_output.put_line( 'Original value is		     : ' || cast( nbr_sample1 as varchar2) );
	 nbr_sample2 := nbr_sample1;
	 dbms_output.put_line( 'Value saved as number(5,-4) type     : ' || cast( nbr_sample2 as varchar2) );
	 EXCEPTION
	 WHEN OTHERS THEN
	 dbms_output.put_line(SQLCODE || ':' || SQLERRM);
	 dbms_output.put_line('When trying to convert number ' || cast( nbr_sample1 as varchar2 ) || ' to type number(5,-4)' );
   end;
  --********************************************************************************************************
  --********************************************************************************************************
   declare
 	 nbr_sample1 number := 12345.58 ;
 	 nbr_sample2 type_train.amount%type ; 
 	-- nbr_sample2 number( * , 1 );
	nl char(1) := chr(13);
 	begin
 	 dbms_output.put_line( '###################### Part2 - Sample 01 ###################' );
 	 dbms_output.put_line( '# precision *(38) scale 1  #' );
 	 dbms_output.put_line( '# any amount until 38 in integer part with 1 part in decimal part  #' );
 	 dbms_output.put_line( nl  );
 	 dbms_output.put_line( 'Original value is	       : ' || cast( nbr_sample1 as varchar2) );
 	 nbr_sample2 := nbr_sample1;
 	 dbms_output.put_line( 'Value saved as number(*,1) type: ' || cast( nbr_sample2 as varchar2) );
  end;
--
--#################################################################################################################
--
   declare
         nbr_sample1 number := 0.1 ;
         nbr_sample2 number( 4 , 5 );
        nl char(1) := chr(13);
        begin
         dbms_output.put_line( '###################### Part2 - Sample 02 ###################' );
         dbms_output.put_line( '# requires zero after the decimal point 5 - 4 = 1  #' );
         dbms_output.put_line( nl  );
         dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
         nbr_sample2 := nbr_sample1;
         dbms_output.put_line( 'Value saved as number(*,1) type: ' || cast( nbr_sample2 as varchar2) );
	EXCEPTION
	WHEN OTHERS THEN
	dbms_output.put_line( SQLCODE || ':' || SQLERRM );
	dbms_output.put_line( 'When trying to convert nbr ' || cast( nbr_sample1 as varchar2 ) || ' to number(4,5)' ) ;
  end;
--
--#################################################################################################################
--
--#################################################################################################################
--
   declare
         nbr_sample1 number := 0.01234567 ;
         nbr_sample2 number( 4 , 5 );
        nl char(1) := chr(13);
        begin
         dbms_output.put_line( '###################### Part2 - Sample 03 ###################' );
         dbms_output.put_line( '# four digits after the zero and decimal point  #' );
         dbms_output.put_line( nl  );
         dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
         nbr_sample2 := nbr_sample1;
         dbms_output.put_line( 'Value saved as number(*,1) type: ' || cast( nbr_sample2 as varchar2) );
  end;
--
--#################################################################################################################
--
--
   declare
         nbr_sample1 number := 0.09999 ;
         nbr_sample2 number( 4 , 5 );
        nl char(1) := chr(13);
        begin
         dbms_output.put_line( '###################### Part2 - Sample 04 ###################' );
         dbms_output.put_line( '# stored as it is  #' );
         dbms_output.put_line( nl  );
         dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
         nbr_sample2 := nbr_sample1;
         dbms_output.put_line( 'Value saved as number(*,1) type: ' || cast( nbr_sample2 as varchar2) );
  end;
--
--#################################################################################################################
--
--
   declare
         nbr_sample1 number := 0.099996 ;
         nbr_sample2 number( 4 , 5 );
        nl char(1) := chr(13);
        begin
         dbms_output.put_line( '###################### Part2 - Sample 05 ###################' );
         dbms_output.put_line( '# stored as it is  #' );
         dbms_output.put_line( nl  );
         dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
         nbr_sample2 := nbr_sample1;
         dbms_output.put_line( 'Value saved as number(*,1) type: ' || cast( nbr_sample2 as varchar2) );
        EXCEPTION
        WHEN OTHERS THEN
        dbms_output.put_line( SQLCODE || ':' || SQLERRM );
        dbms_output.put_line( 'When trying to convert nbr ' || cast( nbr_sample1 as varchar2 ) || ' to number(4,5)' ) ;
  end;
--
--#################################################################################################################
--
--
   declare
         nbr_sample1 number := 0.0012345 ;
         nbr_sample2 number( 2 , 4 );
        nl char(1) := chr(13);
        begin
         dbms_output.put_line( '###################### Part2 - Sample 06 ###################' );
         dbms_output.put_line( '# number(2,4) requires two zeros and two digits after decimal point  #' );
         dbms_output.put_line( nl  );
	 dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
         nbr_sample2 := nbr_sample1;
         dbms_output.put_line( 'Value saved as number(2,4) type: ' || cast( nbr_sample2 as varchar2) );
	nbr_sample1  := 0.003 ;
        dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
        nbr_sample2 := nbr_sample1;
        dbms_output.put_line( 'Value saved as number(2,4) type: ' || cast( nbr_sample2 as varchar2) );
        nbr_sample1  := 0.00999 ;            
        dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
        nbr_sample2 := nbr_sample1;
        dbms_output.put_line( 'Value saved as number(2,4) type: ' || cast( nbr_sample2 as varchar2) );
        EXCEPTION
        WHEN OTHERS THEN
        dbms_output.put_line( SQLCODE || ':' || SQLERRM );
        dbms_output.put_line( 'When trying to convert nbr ' || cast( nbr_sample1 as varchar2 ) || ' to number(2,4)' ) ;
  end;
--
--#################################################################################################################
--
end;
/
