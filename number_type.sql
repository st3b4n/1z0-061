begin
 declare
  nbr_sample1 number:=123.2564;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 01 ###################' );
  dbms_output.put_line( '# precision and scale is not defined, being the default  #' );
  dbms_output.put_line( '# the maximum value  #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( cast( nbr_sample1  as varchar2 ) );
end;
declare
  nbr_sample1 number(6,2) := 1234.9876;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 02 ###################' );
  dbms_output.put_line( '# precision 6 and scale 2  #' );
  dbms_output.put_line( '# so number will be rounded to two digits #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( cast( nbr_sample1  as varchar2 ) );
end;
declare
  nbr_sample1 number(6,2) ;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 03 ###################' );
  dbms_output.put_line( '# precision 6 and scale 2  #' );
  dbms_output.put_line( '# error integer part is out range -9999 - 9999 #' );
  dbms_output.put_line( nl  );
  nbr_sample1 := 12345.12345;
  dbms_output.put_line( cast( nbr_sample1  as varchar2 ) );
  EXCEPTION
  WHEN OTHERS THEN
  	dbms_output.put_line( SQLCODE || ':' || SQLERRM );
end;
declare
  nbr_sample1 number(6,2) ;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 04 ###################' );
  dbms_output.put_line( '# precision 6 and scale 2  #' );
  dbms_output.put_line( '# error integer part is out range -9999 - 9999 #' );
  dbms_output.put_line( nl  );
  nbr_sample1 := 123456;
  dbms_output.put_line( cast( nbr_sample1  as varchar2 ) );
  EXCEPTION
  WHEN OTHERS THEN
        dbms_output.put_line( SQLCODE || ':' || SQLERRM );
end;
declare
  nbr_sample1 number := 1234.9876 ;
  nbr_sample2 number(6) := nbr_sample1;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 05 ###################' );
  dbms_output.put_line( '# precision 6  #' );
  dbms_output.put_line( '# It is rounded to the next integer #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( 'Original value is		: ' || cast( nbr_sample1 as varchar2) );  
  dbms_output.put_line( 'Value saved as number(6) type	: ' || cast( nbr_sample2 as varchar2) );
end;
declare
  nbr_sample1 number := 123456.1 ;
  nbr_sample2 number(6) := nbr_sample1;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 06 ###################' );
  dbms_output.put_line( '# precision 6  #' );
  dbms_output.put_line( '# the decimal part is rounded #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( 'Original value is              : ' || cast( nbr_sample1 as varchar2) );
  dbms_output.put_line( 'Value saved as number(6) type  : ' || cast( nbr_sample2 as varchar2) );
end;
declare
  nbr_sample1 number := 12345.345 ;
  nbr_sample2 number(5,-2) := nbr_sample1;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 07 ###################' );
  dbms_output.put_line( '# precision 5 scale -2  #' );
  dbms_output.put_line( '# negative scale round to the left of the decimal point #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( 'Original value is                 : ' || cast( nbr_sample1 as varchar2) );
  dbms_output.put_line( 'Value saved as number(5,-2) type  : ' || cast( nbr_sample2 as varchar2) );
end;
declare
  nbr_sample1 number := 1234567 ;
  nbr_sample2 number(5,-2) := nbr_sample1;
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 08 ###################' );
  dbms_output.put_line( '# precision 5 scale -2  #' );
  dbms_output.put_line( '# round to the nearest hundred #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( 'Original value is                 : ' || cast( nbr_sample1 as varchar2) );
  dbms_output.put_line( 'Value saved as number(5,-2) type  : ' || cast( nbr_sample2 as varchar2) );
end;
declare
  nbr_sample1 number := 12345678 ;
  nbr_sample2 number(5,-2);
  nl char(1) := chr(13);
 begin
  dbms_output.put_line( '###################### number sample 09 ###################' );
  dbms_output.put_line( '# precision 5 scale -2  #' );
  dbms_output.put_line( '# outside the range, can have only 7 digits in the integer part #' );
  dbms_output.put_line( nl  );
  dbms_output.put_line( 'Original value is                 : ' || cast( nbr_sample1 as varchar2) );
  nbr_sample2 := nbr_sample1;
  dbms_output.put_line( 'Value saved as number(5,-2) type  : ' || cast( nbr_sample2 as varchar2) );
  EXCEPTION
  WHEN OTHERS THEN
  dbms_output.put_line( SQLCODE || ':' || SQLERRM);
  dbms_output.put_line( 'When number ' || cast(nbr_sample1 as varchar2) || ' was tried to be converted to number(5,-2) type');
end;
end;
/
