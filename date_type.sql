--select  sysdate from dual
declare
date_var date := sysdate;
stampdate_var timestamp := systimestamp;
begin
	dbms_output.put_line(date_var);
	dbms_output.put_line(stampdate_var);
end;
/
