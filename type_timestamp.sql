begin
  declare
	 flag integer;
	 l_tname varchar2(100) := 'TRAIN_TIMESTAMP';
  begin
	 select count(1)
	 into flag
	 from tab
	 where tname = l_tname;
	 if flag = 1 then
  	dbms_output.put_line('Table ' || l_tname || ' exist.');
   	execute immediate('truncate table ' || l_tname);
  	dbms_output.put_line('Table ' || l_tname || ' truncated.');
 else
  	dbms_output.put_line('Table ' || l_tname || ' doesn''t exist.');
  	execute immediate('create table ' || l_tname || '(a int, b timestamp)');
  	dbms_output.put_line('Table ' || l_tname || ' created.');
	 end if;
  end;
  --
##################################sssssssssssssssssssssssssss ssss
#####################################################################################
    declare
    l_date timestamp;
    l_date_str   varchar2(100) := '06/12/2010';
    begin	
	dbms_output.put_line('############# S A M P L E - 0 1 ###################');
  	l_date := to_timestamp(l_date_str, 'MM/DD/YYYY');
  	dbms_output.put_line('TimeStamp stored as string:' || l_date_str);
  	dbms_output.put_line('TimeStamp type value after converting using MM/DD/YYYY format:');
  	dbms_output.put_line(l_date);	
    	  insert into TRAIN_TIMESTAMP
  	  values(1,l_date);
    	  commit;
    end;
  --####################################################################################
end;
/
