/*
Table 6-2. Date Formatting Characters
Character Description
MM Represents the numeric month.
MON Represents an abbreviated month name.
MONTH Represents the entire month name.
DD Represents the numeric day of the month.
DY Abbreviation representing the day of the week.
YY Represents the two-digit year.
YYYY Represents the four-digit year.
RR Represents the rounded two-digit year. The year is rounded in the range 1950 to 2049 to
assist with two-digit years such as 10. A two-digit year less than 50 will result in a four-
digit year such as 2010.
AM or PM Represents the meridian indicator.
HH Represents the hour of the day in 12-hour time format.
HH24 Represents the hour of the day in 24-hour time format.
MI Represents the minutes in time.
SS Represents the seconds in time
*/  
begin
  --######################################################################################
  declare
	 flag integer;
	 l_tname varchar2(100) := 'TRAIN_DATE2';
  begin
	 select count(1)
	 into flag
	 from tab
	 where tname = l_tname;
	 if flag = 1 then
  	dbms_output.put_line('Table ' || l_tname || ' exist.');
   	execute immediate('truncate table ' || l_tname);
  	dbms_output.put_line('Table ' || l_tname || ' truncated.');
 else
  	dbms_output.put_line('Table ' || l_tname || ' doesn''t exist.');
  	execute immediate('create table ' || l_tname || '(a int, b date)');
  	dbms_output.put_line('Table ' || l_tname || ' created.');
	 end if;
  end;
  --#####################################################################################
  begin
  insert into TRAIN_DATE2
  values(1,sysdate);
  commit;
  end;
  --####################################################################################
    declare
    l_date date;
    l_date_str   varchar2(100) := '06/12/2010';
    begin	
	dbms_output.put_line('############# S A M P L E - 0 1 ###################');
  	l_date := to_date(l_date_str, 'MM/DD/YYYY');
  	dbms_output.put_line('Date stored as string:' || l_date_str);
  	dbms_output.put_line('Date type value after converting using MM/DD/YYYY format:');
  	dbms_output.put_line(l_date);	
    	  insert into TRAIN_DATE2
  	  values(2,l_date);
    	  commit;
    end;
    --####################################################################################
    declare
    l_date date;
    l_date_str   varchar2(100) := 'December 31, 2010';
    begin
        dbms_output.put_line('############# S A M P L E - 0 2 ###################');
        l_date := to_date(l_date_str, 'Month DD, YYYY');
        dbms_output.put_line('Date stored as string:' || l_date_str);
        dbms_output.put_line('Date type value after converting using ''Month DD, YYYY'' format:');
        dbms_output.put_line(l_date);
          insert into TRAIN_DATE2
          values(3,l_date);
          commit;
    end;
    --####################################################################################
    declare
    l_date date;
    l_date_str   varchar2(100) := 'December 31, 2010 07:35AM';
    begin
        dbms_output.put_line('############# S A M P L E - 0 3 ###################');
        l_date := to_date(l_date_str, 'Month DD, YYYY HH:MIAM');
        dbms_output.put_line('Date stored as string:' || l_date_str);
        dbms_output.put_line('Date type value after converting using ''Month DD, YYYY  HH:MIAM'' format:');
        dbms_output.put_line(l_date);
	dbms_output.put_line('Date type value with mask ''MM/DD/YYYY HH:MIAM'':');
        dbms_output.put_line(to_char(l_date,'MM/DD/YYYY HH:MIAM'));
          insert into TRAIN_DATE2
          values(4,l_date);
          commit;
    end;
   --####################################################################################
    declare
    l_date date;
    l_date_str   varchar2(100) := 'December 31, 2010 20:35:30';
    begin
        dbms_output.put_line('############# S A M P L E - 0 4 ###################');
        l_date := to_date(l_date_str, 'Month DD, YYYY HH24:MI:SS');
        dbms_output.put_line('Date stored as string:' || l_date_str);
        dbms_output.put_line('Date type value after converting using ''Month DD, YYYY HH24:MI:SS'' format:');
        dbms_output.put_line(l_date);
        dbms_output.put_line('Date type value with mask ''MM/DD/YYYY HH:MI:SSAM'':');
        dbms_output.put_line(to_char(l_date,'MM/DD/YYYY HH:MI:SSAM'));
          insert into TRAIN_DATE2
          values(5,l_date);
          commit;
    end;
   --####################################################################################
-- Continue checking link http://infolab.stanford.edu/~ullman/fcdb/oracle/or-time.html
end;
/
