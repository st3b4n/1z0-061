  declare
  l_char char := 'a';
  l_char2 char(2) := 'ab';
  nl char(1) := chr(13);
  begin
    dbms_output.put_line( '###################### char sample 01 ###################' );
    dbms_output.put_line( '# default char size is 1	#' );
    dbms_output.put_line( nl  );
    dbms_output.put_line( 'char defined as char without length:' || l_char );
    l_char := l_char2;
	 EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line( SQLCODE || ':' || SQLERRM);
    dbms_output.put_line( 'Error trying to assign ' || '''' || l_char2 || ''' ' || 'to type defined as ''char''' );
 end;
/
