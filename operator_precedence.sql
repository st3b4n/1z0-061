-- Unary operators has highest precedence
-- select -2*5 from dual
-- Multiplication and division has middle precedence
--select 1 + 6/2 from dual
-- Addition substraccion & concatenation has lowest precedence
-- select 1 + 5 as "Suma", 'My ' || 'name ' || 'is ' || 'Esteban.' as "Presentation" from dual
-- The inner most parenthesis is evaluated first 1 + 2 is evaluated first
select (1+2)*3 as "Compound operation" from dual
/
